# Final Fantasy VII Rebirth Simple Countdown

This is a [simple website](https://breadmaker.gitlab.io/ff7rebirth-simple-countdown/) that displays a countdown for the release of Final Fantasy VII Rebirth with no extra BS.

## Copyright and acknowledgments

The code is shared under the GPLv3 License.

The website uses the "Lifestream Stock" image by [Ravensilver](https://www.deviantart.com/ravensilver). Used under a [Creative Commons Attribute License](https://creativecommons.org/licenses/by/3.0/).

[FINAL FANTASY VII REBIRTH](https://ffvii.square-enix-games.com/en-us/) © SQUARE ENIX CO., LTD. All Rights Reserved.

This work is not affiliated with or endorsed by Square Enix Co., Ltd.
