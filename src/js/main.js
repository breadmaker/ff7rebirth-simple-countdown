console.log('%cHi there! ^_^', 'font-family: Arial, sans-serif; font-weight: bold; font-size: 20px');
console.log('%cWanna know how this was made? Click on the source line over there to know more.', 'font-family: Arial, sans-serif; font-size: 14px');
// Hey there, welcome, let's begin!

// Here, I've created a custom function to emulate a $document.ready() callback
// It ensures logic executes only when everything is properly loaded
const ready = (fn) => {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
};

// This helper function handles the faint animation applied every time a value
// on the countdown changes. It ensures memory is used correctly.
const animateCSS = (element, animation, prefix = 'animate__') =>
  // We create a Promise and return it
  new Promise((resolve, reject) => {
    const animationName = `${prefix}${animation}`;

    element.classList.add(`${prefix}animated`, animationName);
    // When the animation ends, we clean the classes and resolve the Promise
    function handleAnimationEnd(event) {
      event.stopPropagation();
      element.classList.remove(`${prefix}animated`, animationName);
      resolve('Animation ended');
    }
    element.addEventListener('animationend', handleAnimationEnd, {once: true});
  });

// The end of the countdown for FF7 Rebirth, this date is being calculated
// against a fixed time and date regardless of user location.
// Every moving element has been separated and has its own variable.
const targetTime = new Date('2024-02-29T00:00:00').getTime(),
  daysValueElement = document.getElementById('days-value'),
  daysLabelElement = document.getElementById('days-label'),
  hoursValueElement = document.getElementById('hours-value'),
  hoursLabelElement = document.getElementById('hours-label'),
  minutesValueElement = document.getElementById('minutes-value'),
  minutesLabelElement = document.getElementById('minutes-label'),
  secondsValueIntegerElement = document.getElementById('seconds-value-integer'),
  secondsValueFloatElement = document.getElementById('seconds-value-float'),
  secondsLabelElement = document.getElementById('seconds-label');

// This function is set to being executed every time the screen is refreshed
const updateCountdown = () => {
  const currentTime = new Date().getTime(),
    timeDifference = targetTime - currentTime;

  if (timeDifference > 0) {
    const previousDays = parseInt(daysValueElement.innerText),
      previousHours = parseInt(hoursValueElement.innerText),
      previousMinutes = parseInt(minutesValueElement.innerText),
      previousSeconds = parseInt(secondsValueIntegerElement.innerText),
      days = Math.floor(timeDifference / (1000 * 60 * 60 * 24)),
      hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
      minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60)),
      seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);
    if (previousDays !== days) animateCSS(daysValueElement, 'pulse');
    if (previousHours !== hours) animateCSS(hoursValueElement, 'pulse');
    if (previousMinutes !== minutes) animateCSS(minutesValueElement, 'pulse');
    if (previousSeconds !== seconds) animateCSS(secondsValueIntegerElement, 'pulse');
    if (days >= 1) {
      daysValueElement.innerText = days;
      daysLabelElement.classList.toggle('s', days != 1);
    } else if (document.getElementById('days')) {
      document.getElementById('days').remove();
    }
    if (timeDifference > 3600000) {
      hoursValueElement.innerText = hours;
      hoursLabelElement.classList.toggle('s', hours != 1);
    } else if (document.getElementById('hours')) {
      document.getElementById('hours').remove();
    }
    if (timeDifference > 60000) {
      minutesValueElement.innerText = minutes;
      minutesLabelElement.classList.toggle('s', minutes != 1);
    } else if (document.getElementById('minutes')) {
      document.getElementById('minutes').remove();
    }
    secondsValueIntegerElement.innerText = seconds;
    secondsValueFloatElement.classList.toggle('font-size-5vw', timeDifference <= 30000 && timeDifference > 10000);
    secondsValueFloatElement.classList.toggle('font-size-4vw', timeDifference <= 10000);
    if (timeDifference <= 10000) {
      secondsValueFloatElement.innerText = (timeDifference % 1000).toString().padStart(3, '0'); // 4
    } else if (timeDifference <= 30000) {
      secondsValueFloatElement.innerText = (Math.floor((timeDifference % 1000) / 10)).toString().padStart(2, '0'); // 5
    } else if (timeDifference <= 60000) {
      secondsValueFloatElement.innerText = Math.floor((timeDifference % 1000) / 100);
    } else {
      secondsLabelElement.classList.toggle('s', seconds != 1);
    }
    if (timeDifference <= 60000) {
      document.body.style.backgroundColor = 'rgb(' + parseFloat(16 - (timeDifference / 60000) * 16) + ',0,0)';
    }
    // requestAnimationFrame will execute the function in its argument the next time the screen is repainted. This usually occurs when your screen refreshes.
    requestAnimationFrame(updateCountdown);
  } else {
    const CountdownElement = document.getElementById('countdown');
    CountdownElement.textContent = "It's here!";
    CountdownElement.style.fontSize = '15vw';
    CountdownElement.classList.add('fw-bold', 'animate__animated', 'animate__bounceIn');
    document.body.style.backgroundColor = 'rgb(16,0,0)';
  }
  // This will execute every time the screen is resized, as a utility to prevent styling artifacts on some screens.
}, onWindowResize = () => {
  document.documentElement.classList.toggle('h-100', document.documentElement.scrollHeight <= window.innerHeight);
  document.body.classList.toggle('h-100', document.documentElement.scrollHeight <= window.innerHeight);
};

// This will be executed as soon as the document is loaded enough to use this logic with no issues
ready(() => {
  // This helper indicates how long it took to reach this point, starting with
  // console.time('DOMReady'), defined at the very beginning of the document
  console.timeEnd('DOMReady');
  // The following takes care of the loading spinner
  document.getElementById('loading').classList.add('loading-done');
  const currentYear = new Date().getFullYear();
  // This will initialize all bootstrap tooltips
  [...document.querySelectorAll('[data-bs-toggle="tooltip"]')].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));
  // This will initialize the rest
  updateCountdown();
  onWindowResize();
  window.onresize = onWindowResize;
  // This will modify the copyright year, so it updates correctly from 2024 forwards
  if (currentYear > 2023) {
    document.getElementById('year').textContent = ` - ${currentYear}`;
  }
  // I hope this is enough for you to understand. If not, you can DM me at https://twitter.com/BreadMakerCTM or send me a message at https://t.me/breadmaker. Cheers!
});